For a smooth review and merge, please:

1. Fork the repository
2. Open an issue for your feature or bugfix
3. Write a [high quality](https://chris.beams.io/posts/git-commit/) commit message
4. Include tests and verify all tests pass
5. Open a PR referencing the issue from #2

Feel free to [reach out](mailto:message@jared-pri.me) with any questions.

Thank You!