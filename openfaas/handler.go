package openfaas

import (
	"github.com/go-redis/redis/v7"
	"net/http"
	"os"

	"gitlab.com/1ijk/eventime"
	"gitlab.com/1ijk/eventime/api"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	addr, pass := os.Getenv("REDIS_ADDRESS"), os.Getenv("REDIS_PASSWORD")
	zoneinfo, err := eventime.New(addr, eventime.KindRedis)
	if err != eventime.RequireConfiguration {
		http.Error(w, "unable to configure zone information", http.StatusInternalServerError)
		return
	}

	if err := zoneinfo.Configure(redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass, // no password set
	})); err != nil {
		http.Error(w, "unable to configure zone information", http.StatusInternalServerError)
		return
	}

	api.SetZoneInfo(&zoneinfo)

	api.PlanningHandler(w, r)
}
