module gitlab.com/1ijk/go-serverless

go 1.14

require (
	github.com/aws/aws-lambda-go v1.16.0
	github.com/aws/aws-sdk-go v1.30.17
	github.com/go-redis/redis/v7 v7.2.0
	gitlab.com/1ijk/eventime v0.3.2
)
