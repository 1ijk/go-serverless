test:
	@go fmt ./...
	@go vet ./...
	@go test -race -cover ./...

build:
	@go build -o bin/lambda gitlab.com/1ijk/go-serverless/lambda

clean:
	@go mod tidy
	@go clean
	@rm *.zip

packages: clean test
	@GOOS=linux GOARCH=amd64 go build -o bin/lambda gitlab.com/1ijk/go-serverless/lambda
	@zip -r lambda.zip bin/lambda
	@cd cloud-function && zip ../cloud-function.zip *
