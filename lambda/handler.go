package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"gitlab.com/1ijk/eventime"
)

type LambdaRequest struct {
	eventime.EventInfo
}

type LambdaResponse struct {
	Error error          `json:"error:,omitempty"`
	Times eventime.ByGMT `json:"times:"`
}

// Handler provides the business logic that's executed when invoking the AWS Lambda function
// We're leaving the context out of the function signature here, but see https://docs.aws.amazon.com/lambda/latest/dg/golang-context.html
func Handler(event LambdaRequest) (LambdaResponse, error) {
	zoneinfo, err := eventime.New("EventimeZoneInfo", eventime.KindDynamoDB)
	if err != eventime.RequireConfiguration {
		return LambdaResponse{Error: err}, err
	}

	sess, err := session.NewSession(nil)
	if err != nil {
		return LambdaResponse{Error: err}, err
	}

	if err := zoneinfo.Configure(dynamodb.New(sess)); err != nil {
		return LambdaResponse{Error: err}, err
	}

	err = zoneinfo.Load(append(event.EventInfo.GuestCities, event.EventInfo.HostCity)...)
	if err != nil {
		return LambdaResponse{Error: err}, err
	}

	timetable, err := eventime.Plan(event.EventInfo, zoneinfo.Cities())
	if err != nil {
		return LambdaResponse{Error: err}, err
	}

	return LambdaResponse{Times: timetable}, nil
}

func main() {
	lambda.Start(Handler)
}
