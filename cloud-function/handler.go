package cloud_function

import (
	"cloud.google.com/go/storage"
	"context"
	"gitlab.com/1ijk/eventime"
	"gitlab.com/1ijk/eventime/api"
	"net/http"
	"os"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	zoneinfo, err := eventime.New(os.Getenv("BUCKET"), eventime.KindStorage)
	if err != eventime.RequireConfiguration {
		http.Error(w, "unable to configure zone information", http.StatusInternalServerError)
		return
	}
	client, err := storage.NewClient(context.Background())
	if err != nil {
		http.Error(w, "unable to configure zone information", http.StatusInternalServerError)
		return
	}

	if err := zoneinfo.Configure(client); err != nil {
		http.Error(w, "unable to configure zone information", http.StatusInternalServerError)
		return
	}

	api.SetZoneInfo(&zoneinfo)

	api.PlanningHandler(w, r)
}
